import Mock from 'mockjs'

const groups = [];
for (let i = 0; i < 10; i++) {
  groups.push(Mock.mock({
    id: '0010' + i,
    name: '用户组' + i
  }))
}

export {
  groups
}
